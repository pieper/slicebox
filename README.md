SliceBox is a way to open slicer-related data from a shared dropbox folder.

Leverages dropbox for user access control and data management.